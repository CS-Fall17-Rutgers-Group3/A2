#include <vector>
#include <util/Geometry.h>
#include "obstacles/GJK_EPA.h"

#define maxDistance 999999




SteerLib::GJK_EPA::GJK_EPA()
{
}




Util::Vector support(const std::vector<Util::Vector>& mShape, const Util::Vector& v) {
	float highest = std::numeric_limits<float>::min();
	int index = 0;

	for (unsigned int i = 0; i < mShape.size(); i++) {
		Util::Vector t = mShape[i];
		float dot = t*v;
		if (dot > highest) {
			highest = dot;
			index = i;
		}
	}
	return mShape[index];
}

Util::Vector minkowsky(const std::vector<Util::Vector>& _shapeA, const std::vector<Util::Vector>& _shapeB, Util::Vector v)
{
	Util::Vector p1 = support(_shapeA, v);
	Util::Vector p2 = support(_shapeB, -1 * v);
	Util::Vector mkshape = p1 - p2;
	return mkshape;
}
bool CheckOrigin(Util::Vector& v, std::vector<Util::Vector>& simplex) {
	//uses push_back so first simplex in the end of vector 
	Util::Vector s1 = simplex.back();
	// simplex 1 negated 'ns1'
	Util::Vector ns1 = -1 * s1;

	if (simplex.size() == 3) {
		Util::Vector s2 = simplex[1];
		Util::Vector s3 = simplex[0];
		Util::Vector line2 = s2 - s1;
		Util::Vector line3 = s3 - s1;

		//set direction perpendicular to first edge
		v = Util::Vector(line2.z, 0, -1 * line2.x);

		//set direction v to be opposite 
		if (v*s3 > 0) {
			v = -1 * v;
		}

		if (v*ns1 > 0) {
			simplex.erase(simplex.begin() + 0);
			return false;
		}

		v = Util::Vector(line3.z, 0, -1 * line3.x);

		if (v*ns1 > 0) {
			simplex.erase(simplex.begin() + 1);
			return false;
		}
		return true;

	}else{
		//find vector line between first simplex and second simplex
		Util::Vector line = simplex[1] - simplex[0];

		//update direction vector v
		v = Util::Vector(line.z, 0, -1 * line.x);

		//check direction of origin
		if (v*ns1 < 0) {
			v = -1 * v;
		}
		return false;
	}
	
	return false;
}

bool GJK(const std::vector<Util::Vector>& ShapeA, const std::vector<Util::Vector>& ShapeB, std::vector<Util::Vector>& simplex)
{
	Util::Vector DirectionVector(1, 0, -1);
	simplex.push_back(minkowsky(ShapeA, ShapeB, DirectionVector));
	Util::Vector newDirection = -1 * DirectionVector;

	while (true)
	{
		simplex.push_back(minkowsky(ShapeA, ShapeB, newDirection));

		if (simplex.back() * newDirection <= 0)
		{

			return false;
		}
		else
		{
			if (CheckOrigin(newDirection, simplex))
			{
				return true;
			}
		}
	}
}



void nearestEdge(std::vector<Util::Vector>& simplex, float& distance, Util::Vector& normal, int& index)
{
	distance = maxDistance;

	for (int current = 0; current < simplex.size(); current++)
	{
		int next;

		if (current + 1 == simplex.size()) {
			next = 0;
		}
		else {
			next = current + 1;
		}

		Util::Vector vector1 = simplex[current];
		Util::Vector vector2 = simplex[next];
		Util::Vector edge = vector2 - vector1;
		Util::Vector x = vector1*(edge*edge) - edge*(edge*vector1);

		x /= x.norm();

		float distanceFromOriginToEdge = x*vector1;

		if (distanceFromOriginToEdge < distance)
		{
			distance = distanceFromOriginToEdge;
			index = next;
			normal = x;
		}

	}

}

bool EPA(const std::vector<Util::Vector>& shapeA, const std::vector<Util::Vector>& shapeB, std::vector<Util::Vector>& simplex, float& penetration_depth, Util::Vector& penetration_vector)
{
	while (true)
	{
		float distance;
		int index;
		Util::Vector normalVector;

		nearestEdge(simplex, distance, normalVector, index);

		Util::Vector s = minkowsky(shapeA, shapeB, normalVector);
		float dot = s*normalVector;

		if (dot - distance <= 0){
			penetration_vector = normalVector;
			penetration_depth = distance;
			return true;
		}
		else{
			simplex.insert(simplex.begin() + index, s);
		}
	}
}

bool SteerLib::GJK_EPA::intersect(float& return_penetration_depth, Util::Vector& return_penetration_vector, const std::vector<Util::Vector>& _shapeA, const std::vector<Util::Vector>& _shapeB)
{
	bool is_Intersecting;
	std::vector<Util::Vector> Simplex;
	is_Intersecting = GJK(_shapeA, _shapeB, Simplex);

	if (is_Intersecting == true)
	{
		EPA(_shapeA, _shapeB, Simplex, return_penetration_depth, return_penetration_vector);
		Simplex.clear();
		return true;
	}
	else
	{
		Simplex.clear();
		return false;
	}
}